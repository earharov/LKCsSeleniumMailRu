﻿using OpenQA.Selenium;
using System;
using System.Threading.Tasks;

namespace LKTestMailRu.Page_objects
{
    class Login
    {
        //+login

        public Login(IWebDriver driver)
        {
            m_driver = driver;
        }

        public Login Show(String baseurl)
        {
            m_driver.Navigate().GoToUrl(baseurl);

            return this;
        }

        public void Login_admin_user(String user_login, String user_password)
        {
            m_driver.FindElement(By.Id("mailbox__login")).Clear();

            m_driver.FindElement(By.Id("mailbox__login")).SendKeys(user_login);

            m_driver.FindElement(By.Id("mailbox__password")).Clear();

            m_driver.FindElement(By.Id("mailbox__password")).SendKeys(user_password);

            m_driver.FindElement(By.Id("mailbox__auth__button")).Click();

            Task.Delay(3000).Wait();
        }

        private IWebDriver m_driver;
    }
}
