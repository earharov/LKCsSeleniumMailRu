﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System;
using System.Threading.Tasks;

namespace LKTestMailRu.Page_objects
{
    class Main
    {
        //+show itself
        //+open stats page
        //+logout
        //+check stats page open
        //+check warning
        //+close warning
        //+check logged in


        public Main(IWebDriver driver)
        {
            m_driver = driver;
        }

        public Main Show(String baseurl = "https://mail.ru")
        {
            m_driver.Navigate().GoToUrl(baseurl);

            return this;
        }

        public Main Show_create_new_mail_page(String baseurl)
        {
            m_driver.FindElement(By.CssSelector("span.b-toolbar__btn__text.b-toolbar__btn__text_pad")).Click();

            return this;
        }

        public Main Check_sending_mail_page()
        {
            /**
             * Проверяем наличие полей на форме
             * */
            return this;
        }

        public Main Fill_and_send_mail(string to, string subject, string text)
        {
            m_driver.FindElement(By.CssSelector("textarea.js-input.compose__labels__input")).Click();

            m_driver.FindElement(By.CssSelector("textarea.js-input.compose__labels__input")).Clear();

            m_driver.FindElement(By.CssSelector("textarea.js-input.compose__labels__input")).SendKeys(to);

            //Так как скрипт работает очень быстро, иногда может не успеть поймать фокус для следущего шага, поэтому немного подождем
            Task.Delay(1000).Wait();

            m_driver.FindElement(By.Name("Subject")).Click();

            m_driver.FindElement(By.Name("Subject")).Clear();

            m_driver.FindElement(By.Name("Subject")).SendKeys(subject);

            IWebElement we = m_driver.FindElement(By.XPath("//iframe[starts-with(@id,'toolkit-')]"));

            m_driver.SwitchTo().Frame(we);

            m_driver.FindElement(By.Id("tinymce")).Click();

            m_driver.FindElement(By.Id("tinymce")).SendKeys(text);

            m_driver.SwitchTo().DefaultContent();

            m_driver.FindElement(By.XPath("//div[@id='b-toolbar__right']/div[3]/div/div[2]/div/div/span")).Click();

            return this;
        }

        
        public bool Is_logged_in()
        {
            return m_driver.FindElements(By.Id("js-mailbox-exit")).Count != 0;
        }

        public void Logout_user()
        {
            m_driver.FindElement(By.Id("js-mailbox-exit")).Click();
        }

        private IWebDriver m_driver;
    }
}
