﻿using LKTestMailRu.Page_objects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using System;

namespace LKTestMailRu
{
    [TestClass]
    public class UnitTest1
    {
        [TestInitialize]
        public void Setup()
        {
            string browser = "Chrome";
            switch (browser)
            {
                case "Chrome":
                    {
                        m_browser = new ChromeDriver(@"D:\DEV\Lib");
                        break;
                    }
                case "IE":
                    {
                        m_browser = new InternetExplorerDriver(@"D:\DEV\Lib");
                        break;
                    }
                default:
                    {
                        throw new Exception("Неизвестная конфигурация");
                    }
            };
        }

        [TestMethod]
        public void TestMethod1()
        {
            Main mail_page = new Main(m_browser);

            Login login_page = new Login(m_browser);

            login_page
                    .Show("https://mail.ru")

                    .Login_admin_user("testlogin", "testpassword");

            mail_page
                .Show_create_new_mail_page("https://mail.ru")
                .Fill_and_send_mail("test@gmail.com","Some test subject", "Some test text in body");

        }

        [TestCleanup]
        public void TearDown()
        {
            Main mail_page = new Main(m_browser);

            if (mail_page.Is_logged_in())
            {
                mail_page
                    .Show()
                    .Logout_user();
            }

            m_browser.Quit();
        }

        private IWebDriver m_browser;
    }
}
